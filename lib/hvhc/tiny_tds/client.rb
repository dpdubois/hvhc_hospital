# frozen_string_literal: true

require 'singleton'
module Hvhc
  module TinyTds
    class Client
      include Singleton
      attr_reader :connection

      def initialize
        config = ActiveRecord::Base.connection_config
        @connection = ::TinyTds::Client.new(host: config[:host], username: config[:username], password: config[:password],
        port: config[:port], database: config[:database])
      end

      def find_by(table_name, attribute, value)
        result = connection.execute("select * from #{table_name} where #{attribute}=#{value}")
        result.first
      end

      def all(table_name, order_by = nil, offset = nil, fetch_rows = nil)
        unless offset.present? && fetch_rows.present?
          result = connection.execute("select * from #{table_name}")
        else
          result = connection.execute("select * from #{table_name} order by #{order_by} asc offset #{offset} rows fetch next #{fetch_rows} rows only")
        end
        result.each
      end
    end
  end
end
