Rails.application.routes.draw do

  root 'home#index', as: :root

  devise_for :users
  namespace :member do
    resources :hospital_lists do
      collection do
        get :cms_provider
        get :zip_code
      end
    end
  end

  get '*path', to: redirect('/')
end
