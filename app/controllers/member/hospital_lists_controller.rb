# frozen_string_literal: true

class Member::HospitalListsController < ApplicationController
  before_action :find_hospital, only: %i[show edit update destroy]

  def index
    @count = ::Member::HospitalList.count
    @results = ::Member::HospitalList.page params[:page]
    @hospital_lists = @results.first(::Member::HospitalList::PER_PAGE)
  end

  def new
    @hospital_list = ::Member::HospitalList.new
  end

  def edit
  end

  def show
  end

  def create
    @hospital_list = ::Member::HospitalList.new(hospital_params)
    if @hospital_list.save
      flash[:notice] = 'Hospital List was successfully created.'
      redirect_to member_hospital_lists_path
    else
      flash[:alert] = @hospital_list.errors.full_messages.first
      render :new
    end
  end

  def update
    respond_to do |format|
      if @hospital_list.update(hospital_params)
        format.html { redirect_to member_hospital_list_path(@hospital_list.member_id), notice: 'Hospital List was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @hospital_list.destroy
    respond_to do |format|
      format.html { redirect_to member_hospital_lists_path, notice: 'Hospital List was successfully destroyed.' }
    end
  end

  def cms_provider
    provider = CmsProviderOfService.find_by(prvdr_num: params[:prvdr_num])
    render json: { provider: provider, status: 200 }
  end

  def zip_code
    zip_code = Ziphsahrr15.find_by(zipcode15: params[:zip_code])
    render json: { zip_code: zip_code, status: 200 }
  end

  private

  def find_hospital
    @hospital_list = ::Member::HospitalList.where(member_id: params[:id]).first
  end

  def hospital_params
    params.require(:member_hospital_list).permit(:organization_id, :member_id, :prvdr_num_ccn,
                                          :cross_ref_provider_number, :prvdr_ctgry_sbtyp_cd,
                                          :prvdr_ctgry_sbtyp_cd_num, :fac_name_friendly, :fac_name,
                                          :st_adr, :city_name, :state_cd, :zip_cd, :zip_txt,
                                          :aha_sysname, :aha_id, :cms_report, :hrrnum, :hsanum,
                                          :notes, :flagship_hosp)
  end
end
