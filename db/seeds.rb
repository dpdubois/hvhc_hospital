# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

member_hospital_params = {organization_id: "BIDMm", member_id: 5, prvdr_num_ccn: "2200600000",
                          cross_ref_provider_number: "", prvdr_ctgry_sbtyp_cd: "1",
                          prvdr_ctgry_sbtyp_cd_num: 1, fac_name_friendly: "Beth Israel Deaconess Hospital-Plymouth",
                          fac_name: "BETH ISRAEL DEACONESS HOSPITAL - PLYMOUTH", st_adr: "275 SANDWICH",
                          city_name: "PLYMOUTH", state_cd: "MA", zip_cd: 1264, zip_txt: "2360",
                          aha_sysname: "", aha_id: "6141720", cms_report: 1, hrrnum: 227, hsanum: 22052,
                          notes: "", last_date_updated: nil, flagship_hosp: ""}
member_hospital_list = Member::HospitalList.new(member_hospital_params)
member_hospital_list.save!

member_hvhc_dim_params = {member_id: 0.5e1, addr_1: "NA", addr_2: nil, addr_3: nil, city: "Boston",
                          country: "USA", eff_dt: 20120522, exp_dt: nil, phone: "NA", state: "MA",
                          organizational_id: "BIDMC", organizational_name: "Beth Israel Deaconess Medical Center",
                          submit_org_id: "BIDMC", report_org_id: "BIDMC", data_submitter: "Y", submit_type: "SFTP",
                          zip: "NA", member_contact_name: nil, member_contact_phone: nil, member_contact_email: nil,
                          receive_email: nil, color_desc: "Orchid", color_hex_van: "DA70D6", color_rgb_sas: "cxDA70D6",
                          is_a_member: "Y", tech_email_grp: nil}
member_hvhc_dim = Member::HvhcDim.new(member_hvhc_dim_params)
member_hvhc_dim.save!

ziphsahrr15_params = zipcode15: "2360", hsanum: 22052, hsacity: "Plymouth", hsastate: "MA", hrrnum: 227, hrrcity: "Boston", hrrstate: "MA"
Ziphsahrr15.create(ziphsahrr15_params)

cms_provider_params = {prvdr_ctgry_sbtyp_cd: "1", prvdr_ctgry_cd: "1", chow_cnt: "1", chow_dt: nil,
                       city_name: "PLYMOUTH", acptbl_poc_sw: "Y", cmplnc_stus_cd: "A", ssa_cnty_cd: "150",
                       cross_ref_provider_number: nil, crtfctn_dt: "20101207", elgblty_sw: "Y",
                       fac_name: "BETH ISRAEL DEACONESS HOSPITAL - PLYMOUTH", intrmdry_carr_cd: "14201",
                       mdcd_vndr_num: nil, orgnl_prtcptn_dt: "19660701", chow_prior_dt: nil,
                       intrmdry_carr_prior_cd: "14201", prvdr_num: "220060", rgn_cd: "1", skltn_rec_sw: "N",
                       state_cd: "MA", ssa_state_cd: "22", state_rgn_cd: "1", st_adr: "275 SANDWICH STREET",
                       phne_num: "5087462000", pgm_trmntn_cd: "0", trmntn_exprtn_dt: nil, crtfctn_actn_type_cd: "8",
                       gnrl_cntl_type_cd: "2", zip_cd: "2360"}
CmsProviderOfService.create(cms_provider_params)
