$(document).on('turbolinks:load', function() {

  $('#hospital_list').validate({
    errorElement: 'span',
    rules: {
      'member_hospital_list[organization_id]': {
        required: true
      },
      'member_hospital_list[member_id]': {
        required: true
      },
      'member_hospital_list[prvdr_num_ccn]': {
        required: true
      }
    },
    submitHandler: function(form, event) {
      form.submit();
    }
  });

});

function fill_provider_data(data) {
  $('#member_hospital_list_fac_name').val(data.provider.fac_name);
  $('#member_hospital_list_st_adr').val(data.provider.st_adr);
  $('#member_hospital_list_city_name').val(data.provider.city_name);
  $('#member_hospital_list_zip_cd').val(data.provider.zip_cd);
  $('#member_hospital_list_zip_txt').val(data.provider.zip_txt);
  if (!(data.provider.zip_cd == null)) {
    Rails.ajax({
      type: "GET",
      url: "/member/hospital_lists/zip_code",
      data: "zip_code=" + data.provider.zip_cd,
      success: function(repsonse){
        fill_zip_data(repsonse);
      },
      error: function(repsonse){
        alert(repsonse);
      }
    })
  }
}

function fill_zip_data(data) {
  if (!(data.zip_code == null)) {
    $('#member_hospital_list_hrrnum').val(data.zip_code.hrrnum);
    $('#member_hospital_list_hsanum').val(data.zip_code.hsanum);
  }
}

$(function(){
  $("#member_hospital_list_prvdr_num_ccn").change(function(){
    Rails.ajax({
      type: "GET",
      url: "/member/hospital_lists/cms_provider",
      data: "prvdr_num=" + this.value,
      success: function(repsonse){
        fill_provider_data(repsonse);
      },
      error: function(repsonse){
        alert(repsonse);
      }
    })
  });
});
