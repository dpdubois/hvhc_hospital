$(document).on('turbolinks:load', function() {

  $('#new_user').validate({
    errorElement: 'span',
    rules: {
      'user[email]': {
        required: true,
        email: true
      },
      'user[username]': {
        required: true
      },
      'user[role]': {
        required: true
      },
      'user[password]': {
        required: true,
        minlength: 6
      },
      'user[password_confirmation]': {
        required: true,
        minlength: 6
      }
    },
    submitHandler: function(form, event) {
      form.submit();
    }
  });

});
