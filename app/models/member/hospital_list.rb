# frozen_string_literal: true

module Member
  class HospitalList < ApplicationRecord
    self.table_name = 'Member_hospital_list'
    self.primary_keys = :member_id, :prvdr_num_ccn

    paginates_per 2

    PER_PAGE = 2

    validates :organization_id, :member_id, :prvdr_num_ccn, presence: true
    validate :hospital_list

    def hospital_list
      if Member::HospitalList.where(member_id: self.member_id).present? || Member::HospitalList.where(prvdr_num_ccn: self.prvdr_num_ccn).present?
        errors.add(:base, 'Cannot insert duplicate key for member_id or prvdr_num_ccn in object')
      end
    end
  end
end
