# frozen_string_literal: true

module ApplicationHelper

  def page_starts_with(page_no, per_page)
    (page_no * per_page) + 1
  end

  def page_ends_with(page_no, per_page, data_count)
    (page_no * per_page) + data_count
  end
end
