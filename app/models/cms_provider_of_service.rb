# frozen_string_literal: true

class CmsProviderOfService < ApplicationRecord
  self.table_name = 'cms_provider_of_service'
end
