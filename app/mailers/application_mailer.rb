# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'admin@hvhc.com'
  layout 'mailer'
end
