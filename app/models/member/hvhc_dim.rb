# frozen_string_literal: true

module Member
  class HvhcDim < ApplicationRecord
    self.table_name = 'hvhc_dim_member'
  end
end
