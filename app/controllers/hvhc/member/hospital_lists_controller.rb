class Hvhc::Member::HospitalListsController < ApplicationController
  before_action :find_hospital, only: %i[show edit update]

  def index
    @count = ::Member::HospitalList.count
    @results = ::Member::HospitalList.page params[:page]
    @hospital_lists = @results.first(::Member::HospitalList::PER_PAGE)
  end

  def edit
  end

  def show
  end

  def update
    respond_to do |format|
      if @hospital_list.update(hospital_params)
        format.html { redirect_to hvhc_member_hospital_list_path(@hospital_list.member_id), notice: 'Recipe was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  private

  def find_hospital
    @hospital_list = ::Member::HospitalList.where(member_id: params[:id]).first
  end

  def hospital_params
    params.require(:hospital_list).permit(:organization_id, :member_id, :prvdr_num_ccn,
                                          :fac_name_friendly, :fac_name, :st_adr, :city_name,
                                          :zip_cd, :zip_txt, :state_cd, :aha_id)
  end
end
